import React, {Component} from 'react';
import Board from './Board'

export default class Game extends Component{
    constructor(props){
        super(props);
        this.state={
            xIsNext: true,
            stepNumber:0,
            history:[
                {squares:Array(9).fill(null)}
            ]
        }
    }
    jumpTo(step){
        this.setState({
            stepNumber:step,
            xIsNext:(step%2)===0
        })
    }
    handleClick(i){
        let history=this.state.history.slice(0,this.state.stepNumber+1);
        let current=history[history.length-1];
        let squares=current.squares.slice();
        let winner=calculateWinner(squares);
        if(winner||squares[i]){
            return;
        }
        squares[i]=this.state.xIsNext ? 'X' :'O';
        this.setState({
            history: history.concat({
                squares:squares
            }),
            xIsNext:!this.state.xIsNext,
            stepNumber:history.length
        });
    }
    render(){
        let history=this.state.history;
        let current=history[this.state.stepNumber];
        let winner= calculateWinner(current.squares);
        let moves=history.map((step,move)=>{
            let desc=move? 'Turno número: '+move:'Reiniciar juego';
                return(
                    <li key={move}>
                        <button onClick={()=>{this.jumpTo(move)}}>
                            {desc}
                        </button>

                    </li>
                )
        });
        let status;
        if(winner){
            status='El ganador es '+winner;
        }else{
            status='El turno es de '+(this.state.xIsNext ? 'X':'O')
        }
        return(
            <div className="game">
                <div className="game-board">
                    <Board onClick={(i)=>this.handleClick(i)}
                    squares={current.squares}/>
                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <ul>{moves}</ul>
                </div>
            </div>
        )
    }
}
function calculateWinner(squares){
    let lines=[
        [0,1,2],
        [3,4,5],
        [6,7,8],
        [0,3,6],
        [1,4,7],
        [2,5,8],
        [0,4,8],
        [2,4,6]
    ];
    for (let i=0;i<lines.length;i++){
        let [a,b,c]=lines[i];
        if(squares[a] && squares[a]=== squares[b]&& squares[b]=== squares[c]){
            return squares[a];
        }
        
    }
    return null;
}